<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>GIAF</title>
<link rel="stylesheet" type="text/css" href="hoja.css">
<link rel="stylesheet" href="css/login.css?v=<?php echo(rand()); ?>" />
 <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">

<!-- CSS Personalizado -->
<link rel="stylesheet" href="css/Main.css">

<!-- Open-Iconic-->
<link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">

<link rel="stylesheet" href="css/main.css">
</head>

<body>


<?php
require('navegacion.php');
try {
  include("conexion.php");
  // si no pulsó  en el boton de actualizar 
  if (!isset($_POST['btn'])) {

  //guardat en variables los datos que 
      $id=trim($_GET['id']);
      $ciudad=trim($_GET['ciu']);
      $sede=trim($_GET['sed']);
      $responsable=trim($_GET['res']);
      $area=trim($_GET['are']);
      $usuario=trim($_GET['usu']);
      $usuario_red=trim($_GET['usu_red']);
      $propiedad=trim($_GET['pro']);
      $nombre_pc=trim($_GET['nom_pc']);
      $tipo=trim($_GET['tip']);
      $marca=trim($_GET['mar']);
      $modelo=trim($_GET['mod']);
      $placa_pc=trim($_GET['pla_pc']);
      $serial_pc=trim($_GET['ser_pc']);
      $marca_monitor=trim($_GET['mar_mon']);
      $modelo_monitor=trim($_GET['mod_mon']);
      $serial_monitor=trim($_GET['ser_mon']);
      $placa_monitor=trim($_GET['pla_mon']);
      $windows=trim($_GET['win']);
      $office=trim($_GET['off']);
      $fecha_compra=trim($_GET['fec']);
      $depreciacion=trim($_GET['dep']);
      $agente_activo=trim($_GET['age']);
  // si sí puksó el boton de actualizar
  } else {
    $Id=$_POST['Id'];
    $Ciudad=$_POST['Ciu'];
    $Sede=$_POST['Sed'];
    $Responsable=$_POST['Res'];
    $Area=$_POST['Are']; 
    $Usuario=$_POST['Usu'];
    $Usuario_red=$_POST['Usu_Red'];
    $Propiedad=$_POST['Pro'];
    $Nombre_pc=$_POST['Nom_PC'];
    $Tipo=$_POST['Tip'];
    $Marca=$_POST['Mar'];
    $Modelo=$_POST['Mod'];
    $Placa_pc=$_POST['Pla_PC'];
    $Serial_pc=$_POST['Ser_PC'];
    $Marca_monitor=$_POST['Mar_Mon'];
    $Modelo_monitor=$_POST['Mod_Mon'];
    $Serial_monitor=$_POST['Ser_Mon'];
    $Placa_monitor=$_POST['Pla_Mon'];
    $Windows=$_POST['Win'];
    $Office=$_POST['Off'];
    $Fecha_compra=$_POST['Fec'];
    $Depreciacion=$_POST['Dep'];
    $Agente_activo=$_POST['Age'];

    $sql="UPDATE activos SET ID=:Id, CIUDAD=:Ciu, SEDE=:Sed, RESPONSABLE=:Res, AREA=:Are, USUARIO=:Usu, USUARIO_DE_RED=:Usu_Red, PROPIEDAD=:Pro, NOMBRE_PC=:Nom_PC, TIPO=:Tip, MARCA=:Mar, MODELO=:Mod, PLACA_PC=:Pla_PC, SERIAL_PC=:Ser_PC, MARCA_MONITOR=:Mar_Mon, MODELO_MONITOR=:Mod_Mon, SERIAL_MONITOR=:Ser_Mon, PLACA_MONITOR=:Pla_Mon, WINDOW_=:Win, OFFICE=:Of, FECHA_COMPRA=:Fec_Com, DEPRECIACION=:Dep, AGENTE_ACTIVO=:Age_Act WHERE ID=:Id";

    $result=$base-> prepare($sql);

    $result->execute([":Id"=> $Id, ":Ciu" => $Ciudad, ":Sed" => $Sede, ":Res" => $Responsable, ":Are" => $Area, ":Usu" => $Usuario, ":Usu_Red"=>$Usuario_red, ":Pro"=> $Propiedad , ":Nom_PC" => $Nombre_pc, ":Tip" => $Tipo, ":Mar" => $Marca, ":Mod" => $Modelo, ":Pla_PC" => $Placa_pc, ":Ser_PC"=> $Serial_pc, ":Mar_Mon"=> $Marca_monitor , ":Mod_Mon" => $Modelo_monitor, ":Ser_Mon" => $Serial_monitor, ":Pla_Mon" => $Placa_monitor, ":Win" => $Windows, ":Of" => $Office, ":Fec_Com" => $Fecha_compra, ":Dep" => $Depreciacion, ":Age_Act" => $Agente_activo]);

    header("Location:table_activos.php");
  }
}
catch (Exception $e) {
  echo 'Excepción capturada: ',  $e->getMessage(), "\n";
}
?>

<!--funcion $_SERVER-->
<div class="container"> 
    <div class="form-control">
      <h3>ACTUALIZAR</h3>
    </div>
    <br>
  <div class="form-group border">
    <form class name="form1" method="POST" action="<?php $_SERVER["PHP_SELF"] ?>">
      <div class="d-flex flex-wrap form-group"> 
        <div class="from-groud">    
          <div class="col-sm card-proxd form-group">
            <label for="Id_">ID</label>
            <input class="form-control" type="text" name="Id"  value="<?php echo $id ?>">
          </div>
        </div> 
        
        <div class="form-groud">
          <div class="col-sm card-proxd">
            <label for="Ciu_">Ciudad</label> 
            <input class="form-control" type="text" name="Ciu"  value="<?php echo $ciudad ?>">
          </div> 
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd form-group">
            <label for="Sed_">Sede</label> 
            <input class="form-control" type="text" name="Sed" value="<?php echo $sede ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Res_">Resposable</label> 
            <input class="form-control" type="text" name="Res"  value="<?php echo $responsable ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Are_">Área</label> 
            <input class="form-control" type="text" name="Are"  value="<?php echo $area ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Usu_">Usuario</label>
            <input class="form-control" type="text" name="Usu" value="<?php echo $usuario ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Usu_Red_">Usuario Red</label>
              <input class="form-control" type="text" name="Usu_Red"  value="<?php echo $usuario_red?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Pro_">Propiedad</label>
            <input class="form-control" type="text" name="Pro"  value="<?php echo $propiedad ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Nom_PC_">Nombre Pc</label> 
            <input class="form-control" type="text" name="Nom_PC" value="<?php echo $nombre_pc ?>">
          </div> 
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Tip_">Tipo PC</label> 
            <input class="form-control" type="text" name="Tip" value="<?php echo $tipo ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Mar_">Marca</label> 
            <input class="form-control" type="text" name="Mar" value="<?php echo $marca ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Mod_">Modelo</label> 
            <input class="form-control" type="text" name="Mod"  value="<?php echo $modelo ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Pla_PC_">Placa PC</label>
            <input class="form-control" type="text" name="Pla_PC"  value="<?php echo $placa_pc ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Ser_PC_">Serial PC</label>
              <input class="form-control" type="text" name="Ser_PC" value="<?php echo $serial_pc?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Mar_Mon_">Marca Monitor</label> 
            <input class="form-control" type="text" name="Mar_Mon"  value="<?php echo $marca_monitor ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Mod_Mon_">Modelo Monitor</label>
            <input class="form-control" type="text" name="Mod_Mon"  value="<?php echo $modelo_monitor ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Ser_Mon_">Serial Monitor</label>
              <input class="form-control" type="text" name="Ser_Mon" value="<?php echo $serial_monitor?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Pla_Mon_">Placa Monitor</label> 
            <input class="form-control" type="text" name="Pla_Mon" value="<?php echo $placa_monitor ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Win_">Windows</label> 
            <input class="form-control" type="text" name="Win" value="<?php echo $windows ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Off_">Office</label>
            <input class="form-control" type="text" name="Off"  value="<?php echo $office ?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Fec_">Fecha de Compra</label>
              <input class="form-control" type="text" name="Fec" value="<?php echo $fecha_compra?>">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Dep_">Depreciación</label>
              <input class="form-control" type="text" name="Dep"  value="<?php echo $depreciacion?>">
          </div>
        </div>

        <div class="form-group">
          <div class=" col-sm card-proxd">
              <label for="Age_">Agente Activo</label>
              <input class="form-control" type="text" name="Age" value="<?php echo $agente_activo?>">
          </div>
        </div>

        <div class="form-group btn-actualizar">
          <div class="col-sm card-proxd">
            <input class="form-control btn btn-primary" type="submit" name="btn" id="btn" value="Actualizar">
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<p>&nbsp;</p>
    <script src="node_modules/jquery/dist/jquery.min.js" ></script>
    <script src="node_modules/popper.js/dist/popper.min.js" ></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>