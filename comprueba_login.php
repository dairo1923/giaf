<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>GIAF</title>
</head>

<body>
	
	<?php
	
	try{ 
		
		/*secciones 
			
			funcion session_start(): iniciar o reanudar una secion, con get o post o con cookies 
			superglobal $_SESSION[]: 
			
			Despues de hacer esto no deja acceder al contenido
		
		*/
		$base=new PDO('mysql:host=localhost; dbname=inventario','root','');

		$base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
		$base->exec("SET CHARACTER SET utf8");
		

		//Utilizamos marcadores, pueden ser de cualquier nombre
		$sql="SELECT * FROM loginn WHERE USUARIO=:login AND CONTRASENA=:password";
		
		//consulta preparada con marcadores
		$resultado=$base->prepare($sql);
		
		//carateres extraños
		//Rescatar el login y password que el usuario escribio en la otra pagina 
		//htmlentities: convertir cualquier simbolo html, comilla _ 
		//addslashes: Rescapar carateres extraños: evita inyecion
		$login=htmlentities(addslashes($_POST["login"]));
		
		$password=htmlentities(addslashes($_POST["password"]));
		
		//funcion bindvalue: identifica cada marcador para que corresponda con lo que el usuario introdujo
		$resultado->bindValue(":login", $login);
		
		$resultado->bindValue(":password", $password);
		
		$_SESSION['usuario'] = $_POST["login"];
		//asociamos marcadores con variables 
		//No imprimimos en pantalla
		//necesitamos saber si el usuario entro o no con rowCount() 0 o 1 si existe filas
		$resultado->execute();
		
		$numero_registro=$resultado->rowCount();
		
		//pregunto si el usuario si existe
		if($numero_registro!=0){
			
			//si el usuario se encuentra en la bd le creas la sesion
			
			session_start();
			
			//almacenar en la variable session el login del usuario
			//por ser variable superglobal la podemos usar en cualquier pagina de nuestro sitio
			 //$_SESSION["usu"]=$_POST["login"];
			
			$_SESSION['usuario']=$_POST["login"];
			

			header("Location:principal.php");
			
		}else{
		
			//redirigir a una pagina
			header("Location:index.php");
			
		}
		
		
		}catch(Exception $e){
		
			die('Error' . $e->getMessage());
		
		
	}finally{
		
		$base=null;
		
	}
	

	
	?>
	
</body>
</html>