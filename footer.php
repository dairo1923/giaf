<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">           

    <!-- Open-Iconic-->
    <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
	

    <title>GIAF</title>
</head>
<body>
    <footer class="container-fluid text-light bg-dark">
        <div class="d-flex p-4">
            <div class="col-sm">
                <label for="" class="">Created by Dairo Arenas</label>
            </div>
            <div class="col-sm">

            </div>
            <div class="justify-content-sm-end">
                <label for="">Versión 1.0</label>
            </div>
        </div>
    </footer>

    <script src="node_modules/popper.js/dist/popper.min.js" ></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>