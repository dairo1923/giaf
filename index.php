<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>GIAF</title>
<link rel="shortcut icon" href="img/GIAF1.png">
<link rel="stylesheet" href="css/login.css?v=<?php echo(rand()); ?>" />
 <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">

<!-- CSS Personalizado -->
<link rel="stylesheet" href="css/Main.css">

<!-- Open-Iconic-->
<link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
	
</head>

<body>
	
	<div class="container">
		<div class="row vh-100 justify-content-center align-items-center">
			<div class="col-auto bg-darger p-5 text-center">
				<form  action="comprueba_login.php" method="POST" >
				
					<h1>Log In</h1>
					<br>
					<div class="form-floating mb-3">
						<label for="floatingInput">Username</label>
						<input type="text" class="form-control" id="floatingInput" placeholder="Username" name="login">
					</div>
					<div class="form-floating">
						<label for="floatingPassword">Password</label>
						<input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password">
					</div>
					<br>
					<input class="btn btn-outline-primary" type="submit" name="enviando" value="Ingresar">
				</form>
			</div>
			<div class="col-auto bg-darger p-5 text-center">
				<img src="img/GIAF2.png" alt="">
			</div>
		</div>
	</div>
	<script src="node_modules/jquery/dist/jquery.min.js" ></script>
    <script src="js/app.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js" ></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>