<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">

    <!-- CSS Personalizado -->
    <link rel="stylesheet" href="css/Main.css">

    <!-- Open-Iconic-->
    <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">

    <link rel="stylesheet" href="css/main.css">
    <title>Navegación</title>
</head>
<body>
  <?php 
  try {
    include 'conexion.php';

    if (isset($_GET["cerrar"])) {
       header("Location:index.php");
    }
  } catch (Exception $e) {
    die('Error' . $e->getLine());
  } finally{
		$base=null;
	}
	
    
  ?>
<nav class="navbar sticky-top navbar-expand-md navbar-dark bg-dark">
  <img src="img/GIAF.png" alt="" width="75" height="75">
	<a href="" class="navbar-brand"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link" href="principal.php">Inicio</a></li>
          <li class="nav-item"><a class="nav-link" href="table_activos.php">Inventario</a></li>
          <li class="nav-item"><a class="nav-link" href="table_InStore.php">Almacenamiento</a></li>
          <li class="nav-item"><a class="nav-link" href="table_trash.php">Dar de baja</a></li>
        </ul>
      </div>
    <form action="" method="GET">
      <input class="btn btn-danger" type="submit" value="Cerrar" name="cerrar">
    </form>
		</nav>
</body>
</html>