<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">

    <!-- CSS Personalizado -->
    <link rel="stylesheet" href="css/Main.css">

    <!-- Open-Iconic-->
    <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
        
    <title>GIAF</title>
    <link rel="shortcut icon" href="img/GIAF1.png">
</head>
<body>
    <?php 
        include "navegacion.php";
    ?>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-auto  bg-darger p-3 text-center">
                <h1>BIENVENIDO!</h1>
            </div>
        </div>
        <div class="row vh-50 justify-content-center align-items-center">
            <div class="col-auto bg-darger p-6 text-center">
                <div class="card" style="width: 18rem;">
                    <img src="img/inventario.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Inventario</h5>
                        <p class="card-text">Activos en funcionamiento</p>
                        <a href="table_activos.php" class="btn btn-primary">Ingresar</a>
                    </div>
                </div>
            </div>
            <div class="col-auto text-center">
                <div class="card" style="width: 18rem;">
                    <img src="img/instore.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">In Store</h5>
                        <p class="card-text">Activos almacenados</p>
                        <a href="table_InStore.php" class="btn btn-primary">Ingresar</a>
                    </div>
                </div>
            </div>
            <div class="col-auto bg-darger p-6 text-center">
                <div class="card" style="width: 18rem;">
                    <img src="img/basura.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Excluir</h5>
                        <p class="card-text">Activos obsoletos</p>
                        <a href="table_trash.php" class="btn btn-primary">Ingresar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="node_modules/jquery/dist/jquery.min.js" ></script>
    <script src="js/app.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js" ></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>