<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>GIAF</title>
<link rel="shortcut icon" href="img/GIAF1.png">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css" >

 <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">

<!-- CSS Personalizado -->
<link rel="stylesheet" href="css/main.css">

<!-- Open-Iconic-->
<link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
	

</head>

<body>

<?php
require('navegacion.php');
	// ver los registros 
  include 'conexion.php';
	$conexion= $base->query("SELECT * FROM instore");
	
	//manejamos un array de objetos 
	//%registro= un array de objetos 
	$registro=$conexion->fetchAll(PDO::FETCH_OBJ);
	
  //ingresar registros 

  if (isset($_POST['Insert'])) {
    $id=$_POST['Id'];
    $ciudad=$_POST['Ciu'];
    $sede=$_POST['Sed'];
    $responsable=$_POST['Res'];
    $area=$_POST['Are'];
    $usuario=$_POST['Usu'];
    $usuario_red=$_POST['Usu_Red'];
    $propiedad=$_POST['Pro'];
    $nombre_pc=$_POST['Nom_PC'];
    $tipo=$_POST['Tip'];
    $marca=$_POST['Mar'];
    $modelo=$_POST['Mod'];
    $placa_pc=$_POST['Pla'];
    $serial_pc=$_POST['Ser'];
    $marca_monitor=$_POST['Mar_Mon'];
    $modelo_monitor=$_POST['Mod_Mon'];
    $serial_monitor=$_POST['Ser_Mon'];
    $placa_monitor=$_POST['Pla_Mon'];
    $windows=$_POST['Win'];
    $office=$_POST['Off'];
    $fecha_compra=$_POST['Fec'];
    $depreciacion=$_POST['Dep'];
    $agente_activo=$_POST['Age'];

    $sql="INSERT INTO instore (ID, CIUDAD, SEDE, RESPONSABLE, AREA, USUARIO, USUARIO_DE_RED, PROPIEDAD, NOMBRE_PC, TIPO, MARCA, MODELO, PLACA_PC, SERIAL_PC, MARCA_MONITOR, MODELO_MONITOR, SERIAL_MONITOR, PLACA_MONITOR, WINDOW_, OFFICE, FECHA_COMPRA, DEPRECIACION, AGENTE_ACTIVO ) VALUES (:id, :ciu, :sed, :res, :are, :usu, :usu_red, :pro, :nom_pc, :tip, :mar, :mod, :pla_pc, :ser_pc, :mar_mon, :mod_mon, :ser_mon, :pla_mon, :win, :of, :fec_com, :dep, :age_act)";

    $resultado=$base->prepare($sql);

    $resultado->execute(array(":id"=>$id, ":ciu" => $ciudad, ":sed" => $sede, ":res" => $responsable, ":are" => $area, ":usu" => $usuario, ":usu_red"=>$usuario_red, ":pro"=> $propiedad , ":nom_pc" => $nombre_pc, ":tip" => $tipo, ":mar" => $marca, ":mod" => $modelo, ":pla_pc" => $placa_pc, ":ser_pc"=>$serial_pc, ":mar_mon"=> $marca_monitor , ":mod_mon" => $modelo_monitor, ":ser_mon" => $serial_monitor, ":pla_mon" => $placa_monitor, ":win" => $windows, ":of" => $office, ":fec_com" => $fecha_compra, ":dep" => $depreciacion, ":age_act" => $agente_activo));

    header("Location:table_InStore.php");
  }
	
	?>
  
  <div class="container-fluid">
  <h3 class="p-4">Datos</h3>
  <div class="d-flex flex-wrap flex-row ">
    <div class="col-sm-12">
      <form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
      <div class="d-flex flex-wrap form-group"> 
        <div class="from-groud">    
          <div class="col-sm card-proxd form-group">
            <label for="Id_">ID</label>
            <input class="form-control form-control-sm" type="text" name="Id" disabled>
          </div>
        </div> 
        
        <div class="form-groud">
          <div class="col-sm card-proxd">
            <label for="Ciu_">Ciudad</label> 
            <input class="form-control form-control-sm" type="text" name="Ciu">
          </div> 
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd form-group">
            <label for="Sed_">Sede</label> 
            <input class="form-control form-control-sm" type="text" name="Sed">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Res_">Resposable</label> 
            <input class="form-control form-control-sm" type="text" name="Res">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Are_">Área</label> 
            <input class="form-control form-control-sm" type="text" name="Are">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Usu_">Usuario</label>
            <input class="form-control form-control-sm" type="text" name="Usu">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Usu_Red_">Usuario Red</label>
              <input class="form-control form-control-sm" type="text" name="Usu_Red">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Pro_">Propiedad</label>
            <input class="form-control form-control-sm" type="text" name="Pro">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Nom_PC_">Nombre Pc</label> 
            <input class="form-control form-control-sm" type="text" name="Nom_PC">
          </div> 
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Tip_">Tipo PC</label> 
            <input class="form-control form-control-sm" type="text" name="Tip">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Mar_">Marca</label> 
            <select class="form-control form-control-sm" type="text" name="Mar">
              <option value="LENOVO">LENOVO</option>
              <option value="HP">HP</option>
              <option value="ASUS">ASUS</option>
              <option value="HUAWEI">HUAWEI</option>
            </select>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Mod_">Modelo</label> 
            <input class="form-control form-control-sm" type="text" name="Mod">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Pla_PC_">Placa PC</label>
            <input class="form-control form-control-sm" type="text" name="Pla">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Ser_PC_">Serial PC</label>
              <input class="form-control form-control-sm" type="text" name="Ser">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Mar_Mon_">Marca Monitor</label> 
            <input class="form-control form-control-sm" type="text" name="Mar_Mon">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Mod_Mon_">Modelo Monitor</label>
            <input class="form-control form-control-sm" type="text" name="Mod_Mon">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Ser_Mon_">Serial Monitor</label>
              <input class="form-control form-control-sm" type="text" name="Ser_Mon">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Pla_Mon_">Placa Monitor</label> 
            <input class="form-control form-control-sm" type="text" name="Pla_Mon">
          </div>
        </div>

        <div class="form-group ancho-select">
          <div class="col-sm">
            <label for="Win_">Windows</label>
            <select class="form-control form-control-sm" type="text" name="Win">
              <option value="11">11</option>
              <option value="10">10</option>
              <option value="7">7</option>
            </select>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
            <label for="Off_">Office</label>
            <select class="form-control form-control-sm ancho-select" type="text" name="Off">
              <option value="365">365</option>
              <option value="2019">2019</option>
              <option value="2016">2016</option>
            </select>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Fec_">Fecha de Compra</label>
              <input class="form-control form-control-sm" type="text" name="Fec">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm card-proxd">
              <label for="Dep_">Depreciación</label>
              <input class="form-control form-control-sm" type="text" name="Dep">
          </div>
        </div>

        <div class="form-group">
          <div class=" col-sm card-proxd">
              <label for="Age_">Agente Activo</label>
              <input class="form-control form-control-sm" type="text" name="Age">
          </div>
        </div>

        <div class="form-group btn-actualizar">
          <div class="col-sm card-proxd">
            <input class="form-control btn btn-primary " type="submit" name="Insert" id="Insert" value="Insertar">
          </div>
        </div>
      </div>
      
      <div class="p-2">
      <h3>Buscar</h3>
      <input type="text" class="form-control" style="width:30%" id="search" placeholder="Escriba para buscar en la tabla...">
      </div>

        <div class="table-responsive pt-10">
          <table  id="tblData" class="table table-striped table-sm">
            <thead class="">
              <tr>
                <th class="">ID</th>
                <th class="">CIUDAD</th>
                <th class="">SEDE</th>
                <th class="">RESPONSABLE</th>
                <th class="">AREA</th>
                <th class="">USUARIO</th>
                <th class="">USUARIO DE RED</th>
                <th class="">PROPIEDAD</th>
                <th class="">NOMPRE PC</th>
                <th class="">TIPO PC</th>
                <th class="">MARCA PC</th>
                <th class="">MODELO PC</th>
                <th class="">PLACA PC</th>
                <th class="">SERIAL PC</th>
                <th class="">MARCA MONITOR</th>
                <th class="">MODELO MONITOR</th>
                <th class="">SERIAL MONITOR</th>
                <th class="">PLACA MONITOR</th>
                <th class="">WINDOWS</th>
                <th class="">OFFICE</th>
                <th class="">FECHA COMPRA</th>
                <th class="">DEPRECIACION</th>
                <th class="">AGENTE ACTIVO</th>
              </tr> 
            </thead>
              <tbody id="developers">
                <?php
                  
                  foreach($registro as $persona):
                  
                  
                  ?>	
                <tr>
                  <td><?php echo $persona->ID?> </td>
                  <td><?php echo $persona->CIUDAD?></td>
                  <td><?php echo $persona->SEDE?></td>
                  <td><?php echo $persona->RESPONSABLE?></td>
                  <td><?php echo $persona->AREA?></td>
                  <td><?php echo $persona->USUARIO?></td>
                  <td><?php echo $persona->USUARIO_DE_RED?></td>
                  <td><?php echo $persona->PROPIEDAD?> </td>
                  <td><?php echo $persona->NOMBRE_PC?></td>
                  <td><?php echo $persona->TIPO?></td>
                  <td><?php echo $persona->MARCA?></td>
                  <td><?php echo $persona->MODELO?></td>
                  <td><?php echo $persona->PLACA_PC?></td>
                  <td><?php echo $persona->SERIAL_PC?></td>
                  <td><?php echo $persona->MARCA_MONITOR?> </td>
                  <td><?php echo $persona->MODELO_MONITOR?></td>
                  <td><?php echo $persona->SERIAL_MONITOR?></td>
                  <td><?php echo $persona->PLACA_MONITOR?></td>
                  <td><?php echo $persona->WINDOW_?></td>
                  <td><?php echo $persona->OFFICE?></td>
                  <td><?php echo $persona->FECHA_COMPRA?></td>
                  <td><?php echo $persona->DEPRECIACION?> </td>
                  <td><?php echo $persona->AGENTE_ACTIVO?></td>
                  <!--pasamos atrabes de un enlace el id del objeto persona, al pulsar en el boton borrar le pasamos el objeto id y vamos a la pagina eliminar-->
                  <td class="bot"><a href="eliminar.php?ID=<?php echo $persona->ID?>"><img src="https://img.icons8.com/ios/30/000000/trash--v1.png"/></a></td>
                      
                  <td><a href="formulario_editar.php?id=<?php  echo $persona->ID?> 
                  & ciu=<?php  echo $persona->CIUDAD?> 
                  & sed=<?php  echo $persona->SEDE?> 
                  & res=<?php  echo $persona->RESPONSABLE?> 
                  & are=<?php  echo $persona->AREA?> 
                  & usu=<?php  echo $persona->USUARIO?>
                  & usu_red=<?php  echo $persona->USUARIO_DE_RED?> 
                  & pro=<?php  echo $persona->PROPIEDAD?> 
                  & nom_pc=<?php  echo $persona->NOMBRE_PC?> 
                  & tip=<?php  echo $persona->TIPO?> 
                  & mar=<?php  echo $persona->MARCA?> 
                  & mod=<?php  echo $persona->MODELO?>
                  & pla_pc=<?php  echo $persona->PLACA_PC?> 
                  & ser_pc=<?php  echo $persona->SERIAL_PC?> 
                  & mar_mon=<?php  echo $persona->MARCA_MONITOR?> 
                  & mod_mon=<?php  echo $persona->MODELO_MONITOR?> 
                  & ser_mon=<?php  echo $persona->SERIAL_MONITOR?> 
                  & pla_mon=<?php  echo $persona->PLACA_MONITOR?>
                  & win=<?php  echo $persona->WINDOW_?> 
                  & off=<?php  echo $persona->OFFICE?> 
                  & fec=<?php  echo $persona->FECHA_COMPRA?> 
                  & dep=<?php  echo $persona->DEPRECIACION?> 
                  & age=<?php  echo $persona->AGENTE_ACTIVO?>">
                  <img src="https://img.icons8.com/ios/30/000000/refresh.png"/></a></td>   
                </tr>  
                  <?php
                    //cerrar el foreach
                    endforeach;
                    ?>
            </tbody>   
          </table>
        </div>
      </form>
      <div class="form-group">
        <img src="img/excel1.ico" alt="">
        <button class="btn btn-success" onclick="exportTableToExcel('tblData')">Exportar</button>
      </div>
    </div>
  </div>
</div>
<?php 
  include 'footer.php';
?>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <script src="node_modules/jquery/dist/jquery.min.js" ></script>
      <script src="node_modules/popper.js/dist/popper.min.js" ></script>
      <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
      <script src="js/app.js"></script>
      <script>
 // Write on keyup event of keyword input element
        $(document).ready(function(){
        $("#search").keyup(function(){
        _this = this;
        // Show only matching TR, hide rest of them
        $.each($("#tblData tbody tr"), function() {
        if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
        $(this).hide();
        else
        $(this).show();
        });
        });
        });
    </script>
</body>
</html>